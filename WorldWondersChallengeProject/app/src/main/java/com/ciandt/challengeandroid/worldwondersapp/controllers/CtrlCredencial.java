package com.ciandt.challengeandroid.worldwondersapp.controllers;

import android.content.Context;

import com.ciandt.challengeandroid.worldwondersapp.models.Credential;
import com.ciandt.challengeandroid.worldwondersapp.utils.AppConstants;
import com.ciandt.challengeandroid.worldwondersapp.utils.SharedPreferencesUtils;

/**
 * Created by Gustavo on 2015/08/13.
 */
public class CtrlCredencial {
    private final Context mContext;

    public CtrlCredencial(Context context) {
        this.mContext = context;
    }

    public Credential getCredential() {
        if (SharedPreferencesUtils.contains(mContext,
                new String[]{AppConstants.SP_CREDENTAIL_TOKEN_AUTH_KEY,
                        AppConstants.SP_CREDENTAIL_NAME_KEY,
                        AppConstants.SP_CREDENTAIL_ENABLED_KEY})){
            final Credential credential = new Credential();
            credential.setTokenAuth(SharedPreferencesUtils.getString(mContext, AppConstants.SP_CREDENTAIL_TOKEN_AUTH_KEY));
            credential.setName(SharedPreferencesUtils.getString(mContext, AppConstants.SP_CREDENTAIL_NAME_KEY));
            credential.setEnabled(SharedPreferencesUtils.getBoolean(mContext,AppConstants.SP_CREDENTAIL_ENABLED_KEY));

            return credential;
        }

        return null;
    }

    public void saveCredential(final Credential credential) {
        SharedPreferencesUtils.putString(mContext, AppConstants.SP_CREDENTAIL_TOKEN_AUTH_KEY, credential.getTokenAuth());
        SharedPreferencesUtils.putString(mContext, AppConstants.SP_CREDENTAIL_NAME_KEY, credential.getName());
        SharedPreferencesUtils.putBoolean(mContext, AppConstants.SP_CREDENTAIL_ENABLED_KEY, credential.isEnabled());
    }

    public void clearCredential() {
        SharedPreferencesUtils.remove(mContext, AppConstants.SP_CREDENTAIL_TOKEN_AUTH_KEY);
        SharedPreferencesUtils.remove(mContext, AppConstants.SP_CREDENTAIL_NAME_KEY);
        SharedPreferencesUtils.remove(mContext, AppConstants.SP_CREDENTAIL_ENABLED_KEY);
    }
}
