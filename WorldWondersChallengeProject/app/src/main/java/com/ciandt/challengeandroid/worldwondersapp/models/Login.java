package com.ciandt.challengeandroid.worldwondersapp.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Gustavo on 2015/08/13.
 */
public class Login {
    @SerializedName("codigo")
    private Integer mCode;

    @SerializedName("mensagem")
    private String mMessage;

    @SerializedName("dados")
    private Credential mCredential;

    public Integer getCode() {
        return mCode;
    }

    public void setCode(final Integer code) {
        this.mCode = code;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(final String message) {
        this.mMessage = message;
    }

    public Credential getCredential() {
        return mCredential;
    }

    public void setCredential(final Credential credential) {
        this.mCredential = credential;
    }
}
