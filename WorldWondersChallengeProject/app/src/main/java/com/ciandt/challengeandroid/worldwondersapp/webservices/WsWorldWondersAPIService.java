package com.ciandt.challengeandroid.worldwondersapp.webservices;

import com.android.volley.Response;
import com.ciandt.challengeandroid.worldwondersapp.models.Login;
import com.ciandt.challengeandroid.worldwondersapp.models.WorldWonder;
import com.ciandt.challengeandroid.worldwondersapp.models.WorldWonderHolder;

/**
 * Created by Gustavo on 2015/08/13.
 */
public class WsWorldWondersAPIService {
    private static final String SERVICE_URL = "http://private-anon-d4bffabab-worldwondersapi.apiary-mock.com";

    private static final String URL_LOGIN = SERVICE_URL + "/api/v1/login?email=%s&senha=%s";
    private static final String URL_WORLD_WONDERS_COLLECTION = SERVICE_URL + "/api/v1/worldwonders";
    private static final String URL_WORLD_WONDERS_BY_ID = SERVICE_URL + "/api/v1/worldwonders/";

    private static WsWorldWondersAPIService mInstance;

    private final WsServiceController mServiceCtrl;

    private WsWorldWondersAPIService() {
        mServiceCtrl = WsServiceController.getInstance();
    }

    public static WsWorldWondersAPIService getInstance() {
        synchronized (WsWorldWondersAPIService.class) {
            if (mInstance == null) {
                mInstance = new WsWorldWondersAPIService();
            }

            return mInstance;
        }
    }

    public void requestLogin(final String email, final String password, final Response.Listener<Login> listener,
                       final Response.ErrorListener errorListener) {
        final String url = String.format(URL_LOGIN, email, password);
        final WsGsonRequest<Login> req = new WsGsonRequest<Login>(
                url, Login.class, null, listener, errorListener);

        // Add the request to the RequestQueue.
        mServiceCtrl.addToRequestQueue(req);
    }

    public void requestWorldWondersCollection(final Response.Listener<WorldWonderHolder> listener,
                                              final Response.ErrorListener errorListener) {
        final WsGsonRequest<WorldWonderHolder> req = new WsGsonRequest<WorldWonderHolder>(
                URL_WORLD_WONDERS_COLLECTION, WorldWonderHolder.class, null, listener, errorListener);

        // Add the request to the RequestQueue.
        mServiceCtrl.addToRequestQueue(req);
    }

    public void requestWorldWondersById(final int id, final Response.Listener<WorldWonder> listener,
                                        final Response.ErrorListener errorListener) {
        final WsGsonRequest<WorldWonder> req = new WsGsonRequest<WorldWonder>(
                URL_WORLD_WONDERS_BY_ID + id, WorldWonder.class, null, listener, errorListener);

        // Add the request to the RequestQueue.
        mServiceCtrl.addToRequestQueue(req);
    }
}
