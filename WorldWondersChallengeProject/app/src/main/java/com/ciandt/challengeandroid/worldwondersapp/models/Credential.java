package com.ciandt.challengeandroid.worldwondersapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Gustavo on 2015/08/13.
 */
public class Credential implements Serializable {

    private static final long serialVersionUID = -6230300642839144545L;

    @SerializedName("token_auth")
    private String mTokenAuth;

    @SerializedName("nome")
    private String mName;

    @SerializedName("habilitado")
    private boolean mEnabled;

    public String getTokenAuth() {
        return mTokenAuth;
    }

    public void setTokenAuth(final String tokenAuth) {
        this.mTokenAuth = tokenAuth;
    }

    public String getName() {
        return mName;
    }

    public void setName(final String name) {
        this.mName = name;
    }

    public boolean isEnabled() {
        return mEnabled;
    }

    public void setEnabled(final boolean enabled) {
        this.mEnabled = enabled;
    }
}
