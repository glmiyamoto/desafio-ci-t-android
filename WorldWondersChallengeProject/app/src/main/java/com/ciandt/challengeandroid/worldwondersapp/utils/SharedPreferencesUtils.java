package com.ciandt.challengeandroid.worldwondersapp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Gustavo on 2015/08/13.
 */
public class SharedPreferencesUtils {
    private SharedPreferencesUtils() {
        // Avoid instance
    }

    public static boolean putString(final Context context,
                                    final String key, final String value) {
        boolean result;
        try {
            final SharedPreferences sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(context);
            final SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(key, value);
            editor.commit();
            result = true;
        } catch (Exception exception) {
            result = false;
        }
        return result;
    }

    public static boolean putInteger(final Context context,
                                    final String key, final Integer value) {
        boolean result;
        try {
            final SharedPreferences sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(context);
            final SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(key, value);
            editor.commit();
            result = true;
        } catch (Exception exception) {
            result = false;
        }
        return result;
    }

    public static boolean putBoolean(final Context context,
                                     final String key, final Boolean value) {
        boolean result;
        try {
            final SharedPreferences sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(context);
            final SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(key, value ? 1 : 0);
            editor.commit();
            result = true;
        } catch (Exception exception) {
            result = false;
        }
        return result;
    }

    public static String getString(final Context context,
                                   final String key) {
        String result;
        try {
            final SharedPreferences sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(context);
            result = sharedPreferences.getString(key, null);
        } catch (Exception exception) {
            result = null;
        }
        return result;
    }

    public static Integer getInteger(final Context context,
                                     final String key) {
        Integer result;
        try {
            final SharedPreferences sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(context);
            result = sharedPreferences.getInt(key, 0);
        } catch (Exception exception) {
            result = null;
        }
        return result;
    }

    public static boolean getBoolean(final Context context,
                                     final String key) {
        Integer result;
        try {
            final SharedPreferences sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(context);
            result = sharedPreferences.getInt(key, 0);
        } catch (Exception exception) {
            result = null;
        }
        return result == 1;
    }

    public static boolean contains(final Context context,
                                     final String key) {
        boolean result;
        try {
            final SharedPreferences sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(context);
            result = sharedPreferences.contains(key);
        } catch (Exception exception) {
            result = false;
        }
        return result;
    }

    public static boolean contains(final Context context,
                                   final String[] keys) {
        for (String key: keys ) {
            if (!contains(context, key)) {
                return false;
            }
        }

        return true;
    }

    public static boolean remove(final Context context, final String key) {
        boolean result;
        try {
            final SharedPreferences sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(context);
            final SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove(key);
            editor.commit();
            result = true;
        } catch (Exception exception) {
            result = false;
        }
        return result;
    }
}
