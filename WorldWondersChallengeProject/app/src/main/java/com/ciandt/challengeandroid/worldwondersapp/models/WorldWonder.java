package com.ciandt.challengeandroid.worldwondersapp.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Gustavo on 2015/08/13.
 */
public class WorldWonder {
    @SerializedName("id")
    private Integer mId;

    @SerializedName("name")
    private String mName;

    @SerializedName("country")
    private String mCountry;

    @SerializedName("description")
    private String mDescription;

    @SerializedName("image_url")
    private String mImageUrl;

    public Integer getId() {
        return mId;
    }

    public void setId(final Integer id) {
        this.mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(final String name) {
        this.mName = name;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(final String country) {
        this.mCountry = country;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(final String description) {
        this.mDescription = description;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(final String imageUrl) {
        this.mImageUrl = imageUrl;
    }
}
