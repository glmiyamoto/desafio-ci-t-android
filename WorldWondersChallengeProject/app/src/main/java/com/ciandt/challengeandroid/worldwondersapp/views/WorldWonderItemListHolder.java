package com.ciandt.challengeandroid.worldwondersapp.views;

import android.view.View;
import android.widget.TextView;

import com.ciandt.challengeandroid.worldwondersapp.R;

/**
 * Created by Gustavo on 2015/08/13.
 */
public class WorldWonderItemListHolder {
    private TextView mTvName;
    private TextView mTvCountry;
    private TextView mTvDescription;

    public WorldWonderItemListHolder(final View view) {
        mTvName = (TextView) view.findViewById(R.id.tv_name);
        mTvCountry = (TextView) view.findViewById(R.id.tv_country);
        mTvDescription = (TextView) view.findViewById(R.id.tv_description);
    }

    public TextView getTvName() {
        return mTvName;
    }

    public TextView getTvCountry() {
        return mTvCountry;
    }

    public TextView getTvDescription() {
        return mTvDescription;
    }
}
