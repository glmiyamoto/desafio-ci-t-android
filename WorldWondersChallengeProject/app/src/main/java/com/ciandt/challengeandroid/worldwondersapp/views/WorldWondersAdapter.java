package com.ciandt.challengeandroid.worldwondersapp.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.ciandt.challengeandroid.worldwondersapp.R;
import com.ciandt.challengeandroid.worldwondersapp.models.WorldWonder;
import com.ciandt.challengeandroid.worldwondersapp.models.WorldWonderHolder;

/**
 * Created by Gustavo on 2015/08/13.
 */
public class WorldWondersAdapter extends ArrayAdapter {
    private final LayoutInflater mInflater;

    public WorldWondersAdapter(final Context context, final WorldWonderHolder holder) {
        super(context, R.layout.item_list_world_wonder, holder.getWorldWonders());
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int position, final View convertView,
                        final ViewGroup parent) {

        final WorldWonder wonder = (WorldWonder) getItem(position);

        View view = convertView;
        final WorldWonderItemListHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.item_list_world_wonder, parent, false);

            holder = new WorldWonderItemListHolder(view);
            view.setTag(holder);
        } else {
            holder = (WorldWonderItemListHolder) view.getTag();
        }

        // Set values in UI
        holder.getTvName().setText(wonder.getName());
        holder.getTvCountry().setText(wonder.getCountry());
        holder.getTvDescription().setText(wonder.getDescription());

        return view;
    }
}
