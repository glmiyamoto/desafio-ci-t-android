package com.ciandt.challengeandroid.worldwondersapp.utils;

/**
 * Created by Gustavo on 2015/08/13.
 */
public final class AppConstants {
    private AppConstants() {
        // Avoid instance
    }

    // SP - Shared Preference
    public static final String SP_CREDENTAIL_TOKEN_AUTH_KEY = "credential_token_auth_key";
    public static final String SP_CREDENTAIL_NAME_KEY = "credential_name_key";
    public static final String SP_CREDENTAIL_ENABLED_KEY = "credential_enbled_key";

    //
    public static final String LOGIN_CREDENTIAL_EXTRA = "login_credentail_extra";
}
