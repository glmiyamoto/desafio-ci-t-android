package com.ciandt.challengeandroid.worldwondersapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ciandt.challengeandroid.worldwondersapp.controllers.CtrlCredencial;
import com.ciandt.challengeandroid.worldwondersapp.models.Credential;
import com.ciandt.challengeandroid.worldwondersapp.models.WorldWonderHolder;
import com.ciandt.challengeandroid.worldwondersapp.utils.AppConstants;
import com.ciandt.challengeandroid.worldwondersapp.views.WorldWondersAdapter;
import com.ciandt.challengeandroid.worldwondersapp.webservices.WsWorldWondersAPIService;

public class MainActivity extends AppCompatActivity {
    private static final int LOGIN_REQUEST = 0;

    private ListView mLvWorldWonders;

    private CtrlCredencial mCtrlCredencial;
    private WsWorldWondersAPIService mService;

    private Response.Listener<WorldWonderHolder> mWorldWonderListener = new Response.Listener<WorldWonderHolder>() {
        @Override
        public void onResponse(final WorldWonderHolder response) {
            WorldWondersAdapter adapter = new WorldWondersAdapter(MainActivity.this, response);
            mLvWorldWonders.setAdapter(adapter);
        }
    };

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mLvWorldWonders = (ListView) findViewById(R.id.lv_world_wonders);

        mCtrlCredencial = new CtrlCredencial(this);
        mService = WsWorldWondersAPIService.getInstance();

        loadWorldWondersCollection();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        final int id = item.getItemId();

        if (id == R.id.action_logout) {
            logout();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mCtrlCredencial.getCredential() == null) {
            startLoginActivity();
            return;
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LOGIN_REQUEST) {
            if (resultCode == LoginActivity.LOGIN_SUCCESSED_RESULT) {
                final Credential credential = (Credential) data.getSerializableExtra(AppConstants.LOGIN_CREDENTIAL_EXTRA);

                mCtrlCredencial.saveCredential(credential);

                Toast.makeText(this, getString(R.string.login_message, credential.getName()), Toast.LENGTH_SHORT).show();
            } else {
                finish();
            }
        }
    }

    private void startLoginActivity() {
        final Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivityForResult(intent, LOGIN_REQUEST);
    }

    private void logout() {
        mCtrlCredencial.clearCredential();
        startLoginActivity();
    }

    private void loadWorldWondersCollection() {
        mService.requestWorldWondersCollection(mWorldWonderListener, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
