package com.ciandt.challengeandroid.worldwondersapp.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Gustavo on 2015/08/13.
 */
public class WorldWonderHolder {
    @SerializedName("data")
    private WorldWonder[] mWorldWonders;

    public WorldWonder[] getWorldWonders() {
        return mWorldWonders;
    }

    public void setWorldWonders(final WorldWonder[] worldWonders) {
        this.mWorldWonders = worldWonders;
    }
}
